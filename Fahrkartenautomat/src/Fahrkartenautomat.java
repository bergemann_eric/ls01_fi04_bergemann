﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while(true) {
    	rückgeldAusgabe(fahrkartenBezahlen(fahrkartenBestellungErfassen()));
    	fahrkartenAusgabe();
    	delay("\nAutomat bereitet sich für den nächsten Kunden vor!\n\n\n\n\n",2000);
    	}
    }
    
    private static double fahrkartenBestellungErfassen() {
 	   int anzahltickets;
 	   double zuZahlenderBetrag = 0;
 	   int welchesTicket;
 	   Scanner eingabe = new Scanner(System.in);
       double finalBetrag = 0;
       int check = 1;
 	   
 	  double[] preise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
 	  String[] tickets = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
 	   
 	   
 	while(check == 1) {  
 	  System.out.println("Bitte wählen Sie eine Fahrkarte!\n");
 	   for(int i=0;i < preise.length;i++) {
 		   System.out.print("["+ (i+1) +"] ");
 		   System.out.print(tickets[i]);
 		   System.out.printf(" (%.2f€)\n",preise[i]);
 	   }
 	   welchesTicket = eingabe.nextInt();
 	   while(welchesTicket < 1 || welchesTicket > preise.length) {
 		   System.out.println("Falsche Eingabe! Bitte geben Sie einen Wert zwischen 1-10 zur Fahrkartenauswahl ein!\n");
 		  welchesTicket = eingabe.nextInt();
 	   }
 	   
 	   zuZahlenderBetrag = preise[welchesTicket-1];

 	   
 	   System.out.println("\nWie viele Tickets möchten Sie kaufen?");    //wenn man mehrere Tickets kaufen möchte
       anzahltickets = eingabe.nextInt();    //wenn man mehrere Tickets kaufen möchte
       while(anzahltickets > 10 || anzahltickets < 1) {
    	   System.out.println(("Falsche Eingabe! Es können nur 1-10 Tickets auf ein Mal ausgegeben werden! \nBitte geben Sie einen korrekten Wert ein!"));
    	   anzahltickets = eingabe.nextInt();
       }
        
       finalBetrag = finalBetrag + zuZahlenderBetrag*anzahltickets;    //wenn man mehrere Tickets kaufen möchte;
       
       System.out.println("\nMöchten Sie eine weitere Fahrkarte kaufen?");
       System.out.println("[1] Ja");
       System.out.println("[2] Nein");
       check = eingabe.nextInt();
       if(check != 1) {
    	   System.out.println("\n\nSie werden jetzt zum Bezahlungsvorgang weitergeleitet\n\n");
    	   delay("",500);
       }
 	 }
       return finalBetrag;
    }
    
    private static double fahrkartenBezahlen(double finalBetrag) {
    	Scanner geldEingabe = new Scanner(System.in);
    	System.out.printf("Bitte werfen Sie Geld in die Maschine! Sie haben noch %.2f zu zahlen!\nEingabe: ", finalBetrag);
    	double eingezahlterGesamtbetrag = geldEingabe.nextDouble();
    	
    	while(eingezahlterGesamtbetrag != 0.05 && eingezahlterGesamtbetrag != 0.1 && eingezahlterGesamtbetrag != 0.2 && eingezahlterGesamtbetrag != 0.5 && eingezahlterGesamtbetrag != 1 && eingezahlterGesamtbetrag != 2 && eingezahlterGesamtbetrag != 5 && eingezahlterGesamtbetrag != 100) {
   		  	System.out.println("Bitte geben Sie nur Münzen zwischen 5 Cent und 2 Euro oder einen 5 Euro Schein ein!");
    		eingezahlterGesamtbetrag = geldEingabe.nextDouble();
    	}
    	
    	while(eingezahlterGesamtbetrag < finalBetrag)
        {
     	   
     	   System.out.printf("Noch zu zahlen: %.2f €\nEingabe: ", finalBetrag - eingezahlterGesamtbetrag);
     	   double eingeworfeneMünze = geldEingabe.nextDouble();
     	   while(eingeworfeneMünze != 0.05 && eingeworfeneMünze != 0.1 && eingeworfeneMünze != 0.2 && eingeworfeneMünze != 0.5 && eingeworfeneMünze != 1 && eingeworfeneMünze != 2 && eingeworfeneMünze != 5) {
     		  System.out.println("Bitte geben Sie nur Münzen zwischen 5 Cent und 2 Euro oder einen 5 Euro-Schein ein!");
     		  eingeworfeneMünze = geldEingabe.nextDouble();
     	   }
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
           
        }
    	double rückgabeBetrag = eingezahlterGesamtbetrag - finalBetrag;
        return rückgabeBetrag;
    }
    
    private static void fahrkartenAusgabe() {
    	System.out.println("\nFahrschein wird ausgegeben\n");
           delay("    ████████████████████████████████████████████████████████████████████████    \n",100);
           delay("  ██                                                                        ██  \n",100);
           delay("██                                                                            ██\n",100);
           delay("██    ████████████████████████████████████████████████████████████████████    ██\n",100);
           delay("██  ██      ██                                                    ██      ██  ██\n",100);
           delay("██  ██  ██  ██                                                    ██  ██  ██  ██\n",100);
           delay("██  ██      ██  ████████  ██  ██████  ██  ██  ██████  ██████████  ██      ██  ██\n",100);
           delay("██  ██  ██  ██     ██     ██  ██      ██ ██   ██          ██      ██  ██  ██  ██\n",100);
           delay("██  ██      ██     ██     ██  ██      ████    █████       ██      ██      ██  ██\n",100);
           delay("██  ██  ██  ██     ██     ██  ██      ██ ██   ██          ██      ██  ██  ██  ██\n",100);
           delay("██  ██      ██     ██     ██  ██████  ██  ██  ██████      ██      ██      ██  ██\n",100);
           delay("██  ██  ██  ██                                                    ██  ██  ██  ██\n",100);
           delay("██  ██      ██                                                    ██      ██  ██\n",100);
           delay("██    ████████████████████████████████████████████████████████████████████    ██\n",100);
           delay("██                                                                            ██\n",100);
           delay("  ██                                                                        ██  \n",100);
           delay("    ████████████████████████████████████████████████████████████████████████    \n",100);

        System.out.println("\n\n");
    }

















    
    private static void rückgeldAusgabe(double rückgabeBetrag) {
    	
    	int[] münzen= new int[1000];
    	int i=0;
    	int münzenZurückzugeben = 0;
    	if(rückgabeBetrag > 0)
        {
     	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f EURO\n", rückgabeBetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:\n");

            while(rückgabeBetrag >= 1.99) // 2 EURO-Münzen
            {
           	  münzen[i] = 2;
           	  i++;
           	münzenZurückzugeben++;
 	          rückgabeBetrag -= 2.0;
            }
            while(rückgabeBetrag >= 0.99) // 1 EURO-Münzen
            {
            	münzen[i] = 1;
             	  i++;
             	 münzenZurückzugeben++;
 	          rückgabeBetrag -= 1.0;
            }
            while(rückgabeBetrag >= 0.49) // 50 CENT-Münzen
            {
            	münzen[i] = 50;
             	  i++;
             	 münzenZurückzugeben++;
 	          rückgabeBetrag -= 0.5;
            }
            while(rückgabeBetrag >= 0.19) // 20 CENT-Münzen
            {
            	münzen[i] = 20;
             	  i++;
             	 münzenZurückzugeben++;
  	          rückgabeBetrag -= 0.2;
            }
            while(rückgabeBetrag >= 0.09) // 10 CENT-Münzen
            {
            	münzen[i] = 10;
             	  i++;
             	 münzenZurückzugeben++;
 	          rückgabeBetrag -= 0.1;
            }
            while(rückgabeBetrag >= 0.04)// 5 CENT-Münzen
            {
            	münzen[i] = 5;
             	  i++;
             	 münzenZurückzugeben++;
  	          rückgabeBetrag -= 0.05;
            }
            
            delay("",500);
        }
    	i=0;
    	while(münzenZurückzugeben >= 3) {
    		System.out.printf("%5s%2s%-6s","*","*"," *");
    		System.out.printf("%5s%2s%-6s","*","*"," *");
    		System.out.printf("%5s%2s%-6s\n","*","*"," *");

        	System.out.printf("%3s%10s","*","*  ");    		
        	System.out.printf("%3s%10s","*","*  ");
        	System.out.printf("%3s%10s\n","*","*  ");  
        	
        	System.out.printf("%2s%5d%6s","*",münzen[i],"* ");
        	i++;
        	System.out.printf("%2s%5d%6s","*",münzen[i],"* ");
        	i++;
        	System.out.printf("%2s%5d%6s\n","*",münzen[i],"* ");
        	i++;
        	
        	if(münzen[i-3] == 1 || münzen[i-3] == 2) {
        		System.out.printf("%2s%6s%5s","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s","*","Cent","* ");
        	}
        	if(münzen[i-2] == 1 || münzen[i-2] == 2) {
        		System.out.printf("%2s%6s%5s","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s","*","Cent","* ");
        	}
        	if(münzen[i-1] == 1 || münzen[i-1] == 2) {
        		System.out.printf("%2s%6s%5s\n","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s\n","*","Cent","* ");
        	}
        	
        	System.out.printf("%3s%10s","*","*  ");
        	System.out.printf("%3s%10s","*","*  ");
        	System.out.printf("%3s%10s\n","*","*  ");

       	  	System.out.printf("%5s%2s%-6s","*","*"," *");
       	  	System.out.printf("%5s%2s%-6s","*","*"," *");
       	  	System.out.printf("%5s%2s%-6s\n","*","*"," *");

       	  	münzenZurückzugeben = münzenZurückzugeben-3;
       	  
    	}
    	
    	while(münzenZurückzugeben >= 2) {
    		System.out.printf("%5s%2s%-6s","*","*"," *");
    		System.out.printf("%5s%2s%-6s\n","*","*"," *");

        	System.out.printf("%3s%10s","*","*  ");    		
        	System.out.printf("%3s%10s\n","*","*  ");  
        	
        	System.out.printf("%2s%5d%6s","*",münzen[i],"* ");
        	i++;
        	System.out.printf("%2s%5d%6s\n","*",münzen[i],"* ");
        	i++;
        	

        	if(münzen[i-2] == 1 || münzen[i-2] == 2) {
        		System.out.printf("%2s%6s%5s","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s","*","Cent","* ");
        	}
        	if(münzen[i-1] == 1 || münzen[i-1] == 2) {
        		System.out.printf("%2s%6s%5s\n","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s\n","*","Cent","* ");
        	}
        	
        	System.out.printf("%3s%10s","*","*  ");
        	System.out.printf("%3s%10s\n","*","*  ");

       	  	System.out.printf("%5s%2s%-6s","*","*"," *");
       	  	System.out.printf("%5s%2s%-6s\n","*","*"," *");

       	  	münzenZurückzugeben = münzenZurückzugeben-2;
       	  
    	}
    	
    	while(münzenZurückzugeben >= 1) {
    		System.out.printf("%5s%2s%-6s\n","*","*"," *");
		
        	System.out.printf("%3s%10s\n","*","*  ");  
        	
        	System.out.printf("%2s%5d%6s\n","*",münzen[i],"* ");
        	i++;
        	

        	if(münzen[i-1] == 1 || münzen[i-1] == 2) {
        		System.out.printf("%2s%6s%5s\n","*","Euro","* ");
        	}
        	else {
        		System.out.printf("%2s%6s%5s\n","*","Cent","* ");
        	}

        	
        	System.out.printf("%3s%10s\n","*","*  ");

       	  	System.out.printf("%5s%2s%-6s\n","*","*"," *");

       	  	münzenZurückzugeben = münzenZurückzugeben-1;
       	  	

    	}
    

    	delay("",500);
    	
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        
        
     }
    
    private static void delay(String s, long t) {
    	System.out.print(s);
    	try {
 			Thread.sleep(t);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    }
